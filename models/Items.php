<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "items".
 *
 * @property int $id
 * @property string|null $title
 * @property float|null $price
 * @property float|null $old_price
 * @property string|null $description
 * @property string|null $images
 * @property string|null $other_data
 */
class Items extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price', 'old_price'], 'number'],
            [['description', 'images'], 'string'],
            [['other_data'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'price' => 'Price',
            'old_price' => 'Old Price',
            'description' => 'Description',
            'images' => 'Images',
            'other_data' => 'Other Data',
        ];
    }
}
