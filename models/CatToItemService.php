<?php


namespace app\models;


use Yii;

class CatToItemService
{
    public function save($data)
    {
        $data = $this->serialize($data);
        $ins = Yii::$app->db->createCommand()->batchInsert(
                CatToItem::tableName(),
                ['item_id', 'category_id'],
                $data
        )->execute();
        return $ins;


    }

    private function serialize($data)
    {
        $return = [];
        foreach ($data as $key => $value) {
            $attributes = $value->attributes();
            $return[] = [
                    'item_id' => $attributes['id']->__toString(),
                    'category_id' => $value->categoryId->__toString(),
            ];
        }
        return $return;
    }
}