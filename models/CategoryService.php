<?php


namespace app\models;


use Yii;

class CategoryService
{
    public function save($data)
    {
        $data = $this->serialize($data);
        $ins = Yii::$app->db->createCommand()->batchInsert(
                Category::tableName(),
                ['name', 'original_id', 'original_parent_id'],
                $data
        )->execute();
        return $ins;


    }

    private function serialize($data)
    {
        $return = [];
        foreach ($data as $key => $value) {
            $attributes = $value->attributes();

            $return[] = [
                    'name'=>$value->__toString(),
                    'original_id'=>$attributes['id']->__toString(),
                    'original_parent_id'=>isset($attributes['parentId'])?$attributes['parentId']->__toString():NULL
            ];
        }
        return $return;
    }

    public function getCategories()
    {
        return Category::find()->orderBy('name')->all();
    }
}