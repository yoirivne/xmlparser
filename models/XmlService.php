<?php


namespace app\models;


use SimpleXMLElement;
use XMLReader;
use yii\web\NotFoundHttpException;

class XmlService
{
    public function loadCategory($url)
    {
        $nameNode = 'category';
        if (!file_exists($url)) {
            throw new NotFoundHttpException('Файл не найден');
        }
        $categoryService = new CategoryService();
        $z = new XMLReader;
        $z->open($url);
        while ($z->read() && $z->name !== $nameNode) {

        }
        while ($z->name === $nameNode) {
            $node = new SimpleXMLElement($z->readOuterXML());
            $categoryService->save([$node]);
            $z->next($nameNode);
        }
        return true;

    }

    public function loadItem($url)
    {
        $nameNode = 'offer';
        if (!file_exists($url)) {
            throw new NotFoundHttpException('Файл не найден');
        }
        $i=0;$data=[];
        $z = new XMLReader;
        $z->open($url);
        $itemService = new ItemsService();
        $catToItemService = new CatToItemService();
        while ($z->read() && $z->name !== $nameNode) {

        }
        while ($z->name === $nameNode) {
            $data[$i]  =simplexml_load_string($z->readOuterXML(), null, LIBXML_NOCDATA);
            $i++;
            if($i>200){
                $itemService->save($data);
                $catToItemService->save($data);
                unset($data);
                $i=0;
            }
            $z->next($nameNode);
        }
        return true;
    }

}