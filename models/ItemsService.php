<?php


namespace app\models;


use SplFixedArray;
use Yii;
use yii\helpers\Json;

class ItemsService
{
    private $db;
    public function __construct()
    {
        $this->db = Yii::$app->db;
    }


    public function save($data)
    {
        $data = $this->serialize($data);

        $ins = $this->db->createCommand()->batchInsert(
                'items',
                ['id', 'title', 'price', 'old_price', 'description', 'images', 'other_data'],
                $data
        )->execute();

        return true;


    }

    private function serialize($data)
    {
        $return = [];
        foreach ($data as $key => $value) {
            $param = [];
            foreach ($value->param as $val) {
                $param[$val->attributes()->__toString()] = $val->__toString();
            }
            $return[$key] = [
                    'id'=>  $value->attributes()['id']->__toString(),
                    'title'=>  $value->name->__toString(),
                    'price'=> $value->price->__toString(),
                    'old_price'=>  $value->price->__toString(),
                    'description'=> $value->description->__toString(),
                    'images'=> $value->picture->__toString(),
                    'other_data'=>  Json::encode($param),
            ];
        }
        return $return;


    }

}