<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cat_to_item".
 *
 * @property int $id
 * @property string|null $item_id
 * @property float|null $category_id
 */
class CatToItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cat_to_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id'], 'number'],
            [['item_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_id' => 'Item ID',
            'category_id' => 'Category ID',
        ];
    }
}
