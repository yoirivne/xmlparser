<?php


namespace app\models;


use yii\base\Model;
use yii\data\ActiveDataProvider;

class ItemSearch extends Items
{
    public $cat_id;

    public function rules()
    {
        return [
                [['cat_id'], 'string'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param  array  $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Items::find();

        $dataProvider = new ActiveDataProvider([
                'query' => $query,
        ]);

        $this->load($params,'');
        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->innerJoin('cat_to_item', 'items.id = item_id');
        $query->andFilterWhere([
                'category_id' => $this->cat_id,

        ]);
        return $dataProvider;
    }

}