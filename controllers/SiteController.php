<?php

namespace app\controllers;

use app\models\CategoryService;
use app\models\ItemSearch;
use app\models\ItemsService;
use Yii;
use yii\web\Controller;


class SiteController extends Controller
{
   private $categoryService;
   private $itemService;

   public function __construct($id, $module,CategoryService $categoryService,ItemsService  $itemService,$config = [])
   {
       parent::__construct($id, $module, $config);
       $this->categoryService =$categoryService;
       $this->itemService = $itemService;
   }


    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $categories = $this->categoryService->getCategories();
        $searchModel = new ItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index',['categories'=>$categories,'dataProvider'=>$dataProvider]);
    }


}
