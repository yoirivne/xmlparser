<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m210915_060939_category
 */
class m210915_060939_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210915_060939_category cannot be reverted.\n";

        return false;
    }


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('category', [
                'id' => $this->primaryKey(),
                'name' => $this->string()->notNull(),
                'original_id' =>  $this->string(16),
                'original_parent_id'=> $this->string(16),
        ]);

    }

    public function down()
    {
        echo "m210915_060939_category cannot be reverted.\n";
        $this->dropTable('category');
    }

}
