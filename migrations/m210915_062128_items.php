<?php

use yii\db\Migration;

/**
 * Class m210915_062128_items
 */
class m210915_062128_items extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210915_062128_items cannot be reverted.\n";

        return false;
    }


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('items', [
                'id' => $this->string(16),
                'title' => $this->string(),
                'price' =>  $this->money(),
                'old_price' =>  $this->money(),
                'description' =>  $this->text(),
                'images' =>  $this->text(),
                'other_data'=> $this->json(),
        ]);
        $this->addPrimaryKey(
                'item-id',
                'items',
                'id'
        );

    }

    public function down()
    {
        echo "m210915_062128_items cannot be reverted.\n";
        $this->dropTable('items');
    }

}
