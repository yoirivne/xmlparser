<?php

use yii\db\Migration;

/**
 * Class m210915_062931_cat_to_item
 */
class m210915_062931_cat_to_item extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210915_062931_cat_to_item cannot be reverted.\n";

        return false;
    }


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

        $this->createTable('cat_to_item', [
                'id' => $this->primaryKey(),
                'item_id' => $this->string(16),
                'category_id' => $this->string(16),
        ]);
        $this->createIndex(
                'idx-cat_to_item-item_id',
                'cat_to_item',
                'item_id'
        );
        $this->createIndex(
                'idx-cat_to_item-category_id',
                'cat_to_item',
                'category_id'
        );

    }

    public function down()
    {
        echo "m210915_062931_cat_to_item cannot be reverted.\n";
        $this->dropTable('cat_to_item');
    }

}
