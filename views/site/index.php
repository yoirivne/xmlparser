<?php

/* @var $this yii\web\View */
/** @var $category Category*/
/** @var $categories array*/
/** @var $dataProvider ActiveDataProvider*/

use app\models\Category;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\widgets\ListView;

$this->title = 'Каталог товара';
?>
<div class="site-index mt-5">
    <div class="row">
        <div class="col-4">
            <h3>Категории</h3>
            <ul class="list-group">
                <?php foreach ($categories as $category): ?>
                    <li  class="list-group-item">
                        <a href="<?= Url::toRoute(['', 'cat_id' => $category->original_id]); ?>">
                            <?= $category->name; ?>
                        </a>
                    </li>
                <?php endforeach; ?>

            </ul>
        </div>
        <div class="col-8">
            <?= ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemView' => '_list_item',
                    'options' => [
                            'class' => 'list-wrapper row',
                    ],
                    'summaryOptions' => ['class'=>'summary col-12'],
                    'itemOptions' => [
                            'class' => 'col-6',
                    ]
            ]); ?>

        </div>
    </div>


</div>
