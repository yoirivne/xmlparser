
    <div class="card">
        <img src="<?= $model->images; ?>" class="card-img-top" >
        <div class="card-body">
            <h5 class="card-title"><?= $model->title; ?></h5>
            <p class="card-text"><?= $model->description; ?></p>
            <p class="card-text"><?= $model->price; ?></p>
            <table class="table">
                <?php foreach ($model->other_data as $key=>$value):?>
                <tr>
                    <td><?= $key;?></td>
                    <td><?= $value;?></td>
                </tr>
                <?php endforeach;?>

            </table>
        </div>
    </div>
