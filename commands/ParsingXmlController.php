<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\CategoryService;
use app\models\CatToItemService;
use app\models\ItemsService;
use app\models\XmlService;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Url;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ParsingXmlController extends Controller
{
    private $xmlService;
    private $itemsService;
    private $categoryService;
    private $catToItemService;

    public function __construct(
            $id,
            $module,
            XmlService $xmlService,
            ItemsService $itemsService,
            CategoryService $categoryService,
            CatToItemService $catToItemService,
            $config = []
    ) {
        parent::__construct($id, $module, $config);
        $this->xmlService = $xmlService;
        $this->itemsService = $itemsService;
        $this->categoryService = $categoryService;
        $this->catToItemService = $catToItemService;
    }

    /**
     * This command echoes what you have entered as the message.
     *
     * @param $file
     *
     * @return int Exit code
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionLoad()
    {
        //  $url = Yii::getAlias('@app/agent.xml');
        $this->stdout(date('Y-m-d H:i'));
        $url = Yii::getAlias('@app/asos.xml');
        $transaction = Yii::$app->db->beginTransaction();
        if (
                $this->xmlService->loadCategory($url) &&
                $this->xmlService->loadItem($url)
        ) {
            $transaction->commit();
            $this->stdout(date('Y-m-d H:i'));
            return ExitCode::OK;
        } else {
            $transaction->rollBack();
            return ExitCode::DATAERR;
        }


    }
}
